<?php

namespace app\models;

use Yii;


/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $categoryId
 * @property integer $statusId
 *
 * @property Category $category
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'categoryId'], 'required'],
            [['categoryId', 'statusId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['categoryId'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['categoryId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'categoryId' => 'Category ID',
            'statusId' => 'Status ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'categoryId']);
    }
	
	////beforeSave q1c
	public function beforeSave($insert)
   {
       $return = parent::beforeSave($insert);
       if ($this->isNewRecord)
		    $this->statusId = 2;

      return $return; 
   }
}
