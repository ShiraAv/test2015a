<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "status".
 *
 * @property integer $id
 * @property string $name
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
	////Add to dropDown
	public static function getStatus()
	{
		$allStatus = self::find()->all();
		$allStatusArray = ArrayHelper::
					map($allStatus, 'id', 'name');
		return $allStatusArray;						
	}
	////Add to dropDown
	public function getStatusItem()
	{
      return $this->hasOne(Status::className(), ['id' => 'statusId']);
	}
}
