<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Activity[] $activities
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['categoryId' => 'id']);
    }
	////Add to dropDown
	public static function getCategory()
	{
		$allCategory = self::find()->all();
		$allCategoryArray = ArrayHelper::
					map($allCategory, 'id', 'name');
		return $allCategoryArray;						
	}
	////Add to dropDown
	public function getCategoryItem()
	{
      return $this->hasOne(Category::className(), ['id' => 'categoryId']);
	}
	
}
